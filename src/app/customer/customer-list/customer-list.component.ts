import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customerservice/customer.service';
import { MatSnackBar } from '@angular/material';
import { MatDialog } from '@angular/material';
import { CustomerPopupComponent } from '../customer-popup/customer-popup.component';


@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
  
})
export class CustomerListComponent implements OnInit {

  
  public CustomerListData;

  public gridApi;
  public gridColumnApi;
  public currentSelectedData;
  public rowData;

  constructor(private customerService : CustomerService, private snacbar : MatSnackBar ,private dialog:MatDialog) { }

  ngOnInit() {
    
    this.GetCustomerList();
  }

  columnDefs = [
		{headerName: 'CustomerId', field: 'CustomerId' },
		{headerName: 'FirstName ', field: 'FirstName' },
    {headerName: 'Nationality', field: 'Nationality'},
    {headerName: 'Dateofbirth', field: 'Dateofbirth' },
		{headerName: 'Gender ', field: 'Gender' },
		{headerName: 'CustomerStatus ', field: 'CustomerStatus' },
    {headerName: 'Email ', field: 'Email' },
    {headerName: 'MobilePhone ', field: 'MobilePhone' },
	];

  GetCustomerList() : void {
    debugger;    
    this.customerService.GetCustomerList()
      .subscribe( (data : any) => {
        debugger;
        this.rowData = data
      })
  }

  onGridReady(params){
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.customerService.GetCustomerList()
      .subscribe((data:any) => {
        debugger
        this.rowData = data
      });
    console.log(params)
  }

  onSelectionChanged(record) :void {
    let selectedData : any = this.gridApi.getSelectedRows();
      console.log(selectedData);
      if (selectedData) {
        debugger
        this.snacbar.open(selectedData[0].FirstName, 'close', { duration : 700});
      }
  }
 
  openModal(): void {
    const dialogRef = this.dialog.open(CustomerPopupComponent, {});  
    dialogRef.afterClosed().subscribe((data:any)=>{
      this.customerService.CustomerInsert(data)
      .subscribe((response) => {
        debugger;
        alert(response);
      })  
    });
}


}
