import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { TaskCellComponent } from '../tools/task-cell/task-cell.component';
import { TaskPopupComponent } from '../tools/task-popup/task-popup.component';
import { CustomerService } from '../customerservice/customer.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {


  constructor( 
    private http : HttpClient, 
    public snackBar: MatSnackBar, 
    private router : Router,
    private dialog : MatDialog,
    private customerService : CustomerService
    ) { }
  
  ngOnInit( ) {
  }

  public gridApi;
  public gridColumnApi;
  public currentSelectedData;
  public gridOptions = {
    context : {
      componentParent : this
    }
  };

  public rowData;
  

  public columnDefs = [
		{headerName: 'TaskId', field: 'TaskId' , cellRendererFramework : TaskCellComponent,checkboxSelection : true},
		{headerName: 'Title ', field: 'Title' },
    {headerName: 'Description', field: 'Description'},
    {headerName: 'Group', field: 'GroupEn'},
    {headerName: 'Assigned To', field: 'AssignedTo'},
    {headerName: 'Task Assign Employees', field: 'TaskAssignEmployees'},
    {headerName: 'Task Assign Employee Names', field: 'TaskAssignEmployeeNames'},
    {headerName: 'Priority', field: 'PriorityEv'},
    {headerName: 'Target Date', field: 'TargetDate'},
    {headerName: 'Estimated Effort Days', field: 'EstimatedEffortDays'},
    {headerName: 'Estimated Effort Hours', field: 'EstimatedEffortHours'},
    {headerName: 'Estimated Effort Minutes', field: 'EstimatedEffortMinutes'},
    {headerName: 'Scheduled Start', field: 'ScheduledStart'},
    {headerName: 'Scheduled End', field: 'ScheduledEnd'},
    {headerName: 'Actual Start', field: 'ActualStart'},
    {headerName: 'Actual End', field: 'ActualEnd'},
    {headerName: 'Complete Percentage', field: 'CompletePercentage'},
    {headerName: 'Status', field: 'StatusEn'}
  ];
  
  onGridReady(params){
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.GetTaskList();
  }

  GetTaskList(){
    this.customerService.GetTaskList()
      .subscribe((data : any) => {
        this.rowData = data;
      })
  }

  onSelectionChanged() : void {
    debugger;
    let selectedData : any = this.gridApi.getSelectedRows();  
    return selectedData;
  }

  public SelectedRow(int : number) : void {
    this.router.navigate(['/employee-details']);
  }

  public TaskDetails(data) : void {
    debugger
    console.log(data)
    this.router.navigate(['/task-details', data.RecId])
  }

  public EditTask(rowdata) : void {
    debugger;     
    if (rowdata.RecId) {
      debugger
      let recid:string=rowdata.RecId;
      this.customerService.EditTask(recid) 
       .subscribe((data : any) => {
        if(data){
          const dialogRef = this.dialog.open( TaskPopupComponent , {data});
          dialogRef.afterClosed().subscribe(result => {
            debugger
            let assignuser: Array<string> = [result.AssignTo];
                const myobj={
                  ProjectId:result.Project,
                  AssignedTo:assignuser,
                  Priority:result.Priority,
                  Group:result.Group,
                  ScheduledStart:result.ScheduleStart,
                  ScheduledEnd:result.ScheduleEnd,
                  EstimatedEffortDays:result.Days,
                  EstimatedEffortHours:result.Hours,
                  EstimatedEffortMinutes:result.Minutes,
                  Title:result.Title,
                  Description:result.Description,
                  RecId:recid
                }    
                var Task  = JSON.stringify(myobj);
                this.customerService.UpdateTask(Task)
                  .subscribe((data : any) => {
                    if(data.status){
                      this.snackBar.open(data.msg, 'close', { duration : 2000})
                    }
                  })

          })
        }
      })
    }
  }
  public CreatClick():void{
    debugger;
    const dialogRef = this.dialog.open( TaskPopupComponent , { });
    dialogRef.afterClosed().subscribe(result => {
      debugger
  let assignuser: Array<string> = [result.AssignTo];
      const myobj={
        ProjectId:result.Project,
        AssignedTo:assignuser,
        Priority:result.Priority,
        Group:result.Group,
        ScheduledStart:result.ScheduleStart,
        ScheduledEnd:result.ScheduleEnd,
        EstimatedEffortDays:result.Days,
        EstimatedEffortHours:result.Hours,
        EstimatedEffortMinutes:result.Minutes,
        Title:result.Title,
        Description:result.Description
      }    
      var Task  = JSON.stringify(myobj);
      this.customerService.CreateNewTask(Task)
        .subscribe((data : any) => {
          if(data.status){
            this.snackBar.open(data.msg, 'close', { duration : 2000})
          }
        })
      ///CREATE TASK
      console.log(result)
    }); 

  }
}
