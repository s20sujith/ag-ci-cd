import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-employee-details-all-projects',
  templateUrl: './employee-details-all-projects.component.html',
  styleUrls: ['./employee-details-all-projects.component.scss']
})
export class EmployeeDetailsAllProjectsComponent implements OnInit {

  constructor( 
    private http : HttpClient, 
    ) { }
  
  ngOnInit( ) {
    this.http.get('https://jsonplaceholder.typicode.com/todos')
      .subscribe(
        data => this.rowData = data
      );
  }

  public gridApi;
  public gridColumnApi;
  public currentSelectedData;
  public gridOptions = {
    context : {
      componentParent : this
    }
  };

  public rowData;


  public columnDefs = [
		{headerName: 'userId', field: 'userId' },
    {headerName: 'id ', field: 'id' }, 
    {headerName: 'title ', field: 'title' }, 
  	{headerName: 'completed ', field: 'completed' }, ]
}
