import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-employee-details-task-from-current-project',
  templateUrl: './employee-details-task-from-current-project.component.html',
  styleUrls: ['./employee-details-task-from-current-project.component.scss']
})
export class EmployeeDetailsTaskFromCurrentProjectComponent implements OnInit {

  constructor( 
    private http : HttpClient, 
    ) { }
  
  ngOnInit( ) {
    this.http.get('https://jsonplaceholder.typicode.com/todos')
      .subscribe(
        data => this.rowData = data
      );
  }

  public gridApi;
  public gridColumnApi;
  public currentSelectedData;
  public gridOptions = {
    context : {
      componentParent : this
    }
  };

  public rowData;


  public columnDefs = [
		{headerName: 'userId', field: 'userId' },
    {headerName: 'id ', field: 'id' }, 
    {headerName: 'title ', field: 'title' }, 
  	{headerName: 'completed ', field: 'completed' }, ]
}
