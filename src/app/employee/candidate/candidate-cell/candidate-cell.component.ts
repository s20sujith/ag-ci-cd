import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-candidate-cell',
  template: `
  <mat-icon (click)="EditCandidate()">edit</mat-icon> &nbsp;
  <mat-icon (click)="CandidateDetails()">assignment</mat-icon>
  `,
  styles: []
})
export class CandidateCellComponent implements OnInit {

  public data;
  public paramsApi;
  constructor() { }

  agInit(params) {
    this.paramsApi = params;
    this.data = params.value;
  }

  ngOnInit() {

  }

  public CandidateDetails() : void {
    this.paramsApi.context.componentParent.CandidateProcess(this.paramsApi.data);
  }

  public EditCandidate() : void {
    this.paramsApi.context.componentParent.CreateCandidate(this.paramsApi.data);
  } 
}
