import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { EmployeeCustomCellComponent } from '../tools/employee-custom-cell/employee-custom-cell.component';

@Component({
  selector: 'app-job-application-list',
  templateUrl: './job-application-list.component.html',
  styleUrls: ['./job-application-list.component.scss']
})
export class JobApplicationListComponent implements OnInit {


  constructor( 
    private http : HttpClient, 
    public snackBar: MatSnackBar, 
    private router : Router,
    private dialog : MatDialog
    ) { }
  
  ngOnInit( ) {
  }

  public gridApi;
  public gridColumnApi;
  public currentSelectedData;
  public gridOptions = {
    context : {
      componentParent : this
    }
  };
  
  public rowData;
  public columnDefs = [
		{headerName: 'User Id ', field: 'userId' , cellRendererFramework : EmployeeCustomCellComponent, checkboxSelection : true},
		{headerName: 'Title ', field: 'title' },
    {headerName: 'completed', field: 'completed'},
    {headerName: '2nd title ', field: 'title' },
		{headerName: 'Title ', field: 'title' },
		{headerName: 'User Id ', field: 'userId' },
    {headerName: '4tile ', field: 'title' },
    {headerName: 'Title ', field: 'title' },
    {headerName: '6t ', field: 'title',  },
  ];
  
  onGridReady(params){
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.http.get('https://jsonplaceholder.typicode.com/todos').subscribe(d => this.rowData = d)
    console.log(params);
  }
}
