import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../guard/auth-service/auth.service';
import { GlobalServiceService } from '../guard/global-service/global-service.service';
import { LoginService } from './login-service/login.service';
import { MatSnackBar } from '@angular/material';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  constructor(
    private router : Router, 
    private authService : AuthService,
    private globalService  : GlobalServiceService,
    private loginService : LoginService,
    private snakbar : MatSnackBar,
    private http : HttpClient
    ) { }
    
    public LoginStatus : string = 'LoginStatus';
    public LoginUserName : string = 'UserName';
    public LoginUserInfo : string = 'UserInfo';
    public LoginUserToken : string = 'UserToken';

  ngOnInit() {

  }

  loginForm(ngform : any) : void {
    debugger;
    console.log(ngform);
    if(ngform.username && ngform.password){
      let username : string = ngform.username;
      let password : string = ngform.password;

            localStorage.setItem(this.LoginStatus, 'true');
            localStorage.setItem(this.LoginUserName, 'user');
            localStorage.setItem(this.LoginUserToken, '8659864321687984561684');
            localStorage.setItem(this.LoginUserInfo, 'user, emial, token');
            this.globalService.SetLocalValues();
            // this.globalService.SetValue(true);
            this.router.navigate(['/employees'])

      // this.loginService.UserLogin(username, password)
      // this.http.get('http://localhost:60584/api/UserLogin?UserName='+ username +'&Password=' + password)
      //   .subscribe((data:any)=>{
      //     debugger
      //     if(data.status){
      //       localStorage.setItem(this.LoginStatus, data.status);
      //       localStorage.setItem(this.LoginUserName, data.UserInfo.UserLogin.UserName);
      //       localStorage.setItem(this.LoginUserToken, data.UserInfo.tocken);
      //       localStorage.setItem(this.LoginUserInfo, JSON.stringify(data.UserInfo))
      //       this.globalService.SetLocalValues();
      //       this.router.navigate(['/employees'])
      //     }
      //   })
    }
    else{
      this.snakbar.open('Username or Password are invalid!', 'close', {
        duration : 5000
      })
    }
    // this.globalService.SetValue(true);
    // localStorage.setItem('token', 'sdfasdf')
  }

}