import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AgGridModule } from 'ag-grid-angular';
import { FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { LayoutModule } from '@angular/cdk/layout';

import { AdminTemplateComponent } from './admin-template/admin-template.component';
import { AppComponent } from './app.component';
import { AuthGuard } from './guard/auth/auth.guard';
import { LoginComponent } from './login/login.component';
import { CustomerListComponent } from './customer/customer-list/customer-list.component';
import { CustomerDetailsComponent } from './customer/customer-details/customer-details.component';
import { CustomerService } from './customer/customerservice/customer.service';
import { EmployeesComponent } from './employee/employees/employees.component';
import { EmployeeSearchComponent } from './employee/employee-search/employee-search.component';
import { CandidateComponent } from './employee/candidate/candidate.component';
import { JobApplicationListComponent } from './employee/job-application-list/job-application-list.component';
import { ProjectEmployeesComponent } from './employee/project-employees/project-employees.component';
import { EmployeeCustomCellComponent } from './employee/tools/employee-custom-cell/employee-custom-cell.component';
import { EmployeeDetailsComponent } from './employee/employee-details/employee-details.component';
import { EmployeeCreatePopupComponent } from './employee/tools/employee-create-popup/employee-create-popup.component';
import { GlobalServiceService } from './guard/global-service/global-service.service';
import { UsersComponent } from './organization/users/users.component';
import { ProjectsComponent } from './customer/projects/projects.component';
import { CustomersComponent } from './customer/customers/customers.component';
import { TaskComponent } from './customer/task/task.component';
import { OpportunityNewComponent } from './customer/opportunity-new/opportunity-new.component';
import { TaskTemplateComponent } from './customer/task-template/task-template.component';
import { LookUpComponent } from './organization/look-up/look-up.component';
import { PayGroupComponent } from './organization/pay-group/pay-group.component';
import { MenusComponent } from './organization/menus/menus.component';
import { RolesComponent } from './organization/roles/roles.component';
import { PermissionComponent } from './organization/permission/permission.component';
import { StaffComponent } from './organization/staff/staff.component';
import { EnquiryComponent } from './organization/enquiry/enquiry.component';
import { NotificationComponent } from './organization/notification/notification.component';
import { JobSetupComponent } from './organization/job-setup/job-setup.component';
import { TaskCellComponent } from './customer/tools/task-cell/task-cell.component';
import { TaskPopupComponent } from './customer/tools/task-popup/task-popup.component';
import { TaskDetailsComponent } from './customer/task-details/task-details.component';
import { EmployeeDetailsAllTasksComponent } from './employee/tools/employee-details-all-tasks/employee-details-all-tasks.component';
import { EmployeeDetailsAllProjectsComponent } from './employee/tools/employee-details-all-projects/employee-details-all-projects.component';
import { EmployeeDetailsTaskFromCurrentProjectComponent } from './employee/tools/employee-details-task-from-current-project/employee-details-task-from-current-project.component';
import { EmployeeDetailsProfileComponent } from './employee/tools/employee-details-profile/employee-details-profile.component';
import { ProjectEmployeesCreatePopupComponent } from './employee/tools/project-employees-create-popup/project-employees-create-popup.component';
import { CandidateCreatePopupComponent } from './employee/candidate/candidate-create-popup/candidate-create-popup.component';
import { CandidateProcessComponent } from './employee/candidate/candidate-process/candidate-process.component';
import { CandidateCellComponent } from './employee/candidate/candidate-cell/candidate-cell.component';
import { CustomerPopupComponent } from './customer/customer-popup/customer-popup.component';



@NgModule({
  declarations: [
    AppComponent,
    AdminTemplateComponent,
    LoginComponent,
    CustomerListComponent,
    CustomerDetailsComponent,
    EmployeesComponent,
    EmployeeSearchComponent,
    CandidateComponent,
    JobApplicationListComponent,
    ProjectEmployeesComponent,
    EmployeeCustomCellComponent,
    EmployeeDetailsComponent,
    EmployeeCreatePopupComponent,
    UsersComponent,
    ProjectsComponent,
    CustomersComponent,
    TaskComponent,
    OpportunityNewComponent,
    TaskTemplateComponent,
    LookUpComponent,
    PayGroupComponent,
    MenusComponent,
    RolesComponent,
    PermissionComponent,
    StaffComponent,
    EnquiryComponent,
    NotificationComponent,
    JobSetupComponent,
    TaskCellComponent,
    TaskPopupComponent,
    TaskDetailsComponent,
    EmployeeDetailsAllTasksComponent,
    EmployeeDetailsAllProjectsComponent,
    EmployeeDetailsTaskFromCurrentProjectComponent,
    EmployeeDetailsProfileComponent,
    ProjectEmployeesCreatePopupComponent,
    CandidateCreatePopupComponent,
    CandidateProcessComponent,
    CandidateCellComponent,
    CustomerPopupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    LayoutModule,
    HttpClientModule,
    FormsModule,
    AgGridModule.withComponents([

    ])
  ],
  providers: [
   CustomerService,
   AuthGuard,
   GlobalServiceService
  ],
  entryComponents : [
    EmployeeCustomCellComponent,
    TaskCellComponent,
    TaskPopupComponent,
    EmployeeCreatePopupComponent,
    ProjectEmployeesCreatePopupComponent,
    CandidateCreatePopupComponent,
    CandidateCellComponent,
    CustomerPopupComponent
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
