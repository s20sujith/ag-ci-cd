import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { CustomerListComponent } from './customer/customer-list/customer-list.component';
import { CandidateComponent } from './employee/candidate/candidate.component';
import { EmployeeSearchComponent } from './employee/employee-search/employee-search.component';
import { EmployeesComponent } from './employee/employees/employees.component';
import { JobApplicationListComponent } from './employee/job-application-list/job-application-list.component';
import { ProjectEmployeesComponent } from './employee/project-employees/project-employees.component';
import { EmployeeDetailsComponent } from './employee/employee-details/employee-details.component';
import { AuthGuard } from './guard/auth/auth.guard';
import { AppComponent } from './app.component';
import { CustomersComponent } from './customer/customers/customers.component';
import { OpportunityNewComponent } from './customer/opportunity-new/opportunity-new.component';
import { ProjectsComponent } from './customer/projects/projects.component';
import { TaskComponent } from './customer/task/task.component';
import { TaskTemplateComponent } from './customer/task-template/task-template.component';
import { EnquiryComponent } from './organization/enquiry/enquiry.component';
import { JobSetupComponent } from './organization/job-setup/job-setup.component';
import { LookUpComponent } from './organization/look-up/look-up.component';
import { MenusComponent } from './organization/menus/menus.component';
import { NotificationComponent } from './organization/notification/notification.component';
import { PayGroupComponent } from './organization/pay-group/pay-group.component';
import { PermissionComponent } from './organization/permission/permission.component';
import { RolesComponent } from './organization/roles/roles.component';
import { StaffComponent } from './organization/staff/staff.component';
import { UsersComponent } from './organization/users/users.component';
import { TaskDetailsComponent } from './customer/task-details/task-details.component';
import { CandidateProcessComponent } from './employee/candidate/candidate-process/candidate-process.component';

const routes: Routes = [
  { path : '', redirectTo:'/home', pathMatch:'full'},
  { path : 'home', component :AppComponent},
  { path : 'login', component : LoginComponent},

// Employee
  { path : 'candidate', component : CandidateComponent, canActivate : [AuthGuard]},
  { path : 'employee-search', component : EmployeeSearchComponent, canActivate : [AuthGuard]},
  { path : 'employees', component : EmployeesComponent, canActivate : [AuthGuard]},
  { path : 'employee-details/:RecId', component : EmployeeDetailsComponent, canActivate : [AuthGuard]},
  { path : 'job-application-list', component : JobApplicationListComponent, canActivate : [AuthGuard]},
  { path : 'project-employees', component : ProjectEmployeesComponent, canActivate : [AuthGuard]},

// Organization
  { path : 'enquiry', component : EnquiryComponent, canActivate : [AuthGuard]},
  { path : 'job-setup', component : JobSetupComponent, canActivate : [AuthGuard]},
  { path : 'look-up', component : LookUpComponent, canActivate : [AuthGuard]},
  { path : 'menus', component : MenusComponent, canActivate : [AuthGuard]},
  { path : 'notification', component : NotificationComponent, canActivate : [AuthGuard]},
  { path : 'pay-group', component : PayGroupComponent, canActivate : [AuthGuard]},
  { path : 'permission', component : PermissionComponent, canActivate : [AuthGuard]},
  { path : 'roles', component : RolesComponent, canActivate : [AuthGuard]},
  { path : 'staff', component : StaffComponent, canActivate : [AuthGuard]},
  { path : 'users', component : UsersComponent, canActivate : [AuthGuard]},

// Customers
  { path : 'customers', component : CustomersComponent, canActivate : [AuthGuard]},
  { path : 'customer-list', component : CustomerListComponent, canActivate: [AuthGuard]},
  { path : 'opportunity-new', component : OpportunityNewComponent, canActivate : [AuthGuard]},
  { path : 'projects', component : ProjectsComponent, canActivate :[AuthGuard]},
  { path : 'task', component : TaskComponent, canActivate : [AuthGuard] },
  { path : 'task-details/:RecId', component : TaskDetailsComponent, canActivate : [AuthGuard]},
  { path : 'task-template', component : TaskTemplateComponent, canActivate : [AuthGuard]},
  { path : 'candidate-process/:RecId', component : CandidateProcessComponent, canActivate : [AuthGuard]},

  { path : 'customer-list', component : CustomerListComponent, canActivate : [AuthGuard]},
];

@NgModule({
  providers : [AuthGuard],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
